## Preparation:
Before building the image please update the **.gitconfig** with your git **username** and **email** for the https://git.werkbliq.de.

For GitLab authentication the build script uses ssh key authentication method during build, therefore a private key has to be provided prior to building the image.

Generate a key and name it as **id_git_rsa** (or use existing key pair) for GitLab authentication: (If you name it differenctly, please adjust the key copy part of the Dockerfile)

`ssh-keygen -o -t rsa -b 4096`

Copy **only** the **public** key part of the key to your GitLab account at https://git.werkbliq.de, under 'Settings'>'SSH keys'>'Add an SSH key'

Copy **only** the **private** ssh key to the **key** subdirectory of the build directory. (It will be copied to the image */root/.ssh* folder) 

If you'd like to add your custom aliases to the image, add them to the .aliases file, it is also going to be copied to the built image.

## Build (build time ~8-10 minutes): 
`docker build -t az_cli_ubuntu .`

## Usage:
### Run: 
(Set the -v mount flag to your liking)

`docker run -d -v ${PWD}:/cluster --name azure-cli -h azure-cli -it az_cli_ubuntu`

### Enter the cli:
`docker exec -it azure-cli /bin/bash`

### Login to azure cli:
Sometimes the default tenant is set differently when `az login` is executed (because we /are going to/ have access to multiple subscriptions), therefore, defining the tenant can help.

`az login --tenant WERKBLiQGmbH.onmicrosoft.com`

## Installed/supported tools:
- wq (Sebastian Ziemann)
- kubectl
- kubectx
- kubens
- helm v3
- git
- maven (+ openjdk-11-jdk-headless)
- curl
- python3 v3.8.5
- pip3 v20.0.2 

## Self developed tools:
- imagetags.py (Zsolt Kormanyos)
