__parse_git_branch() {
    
    CURRENT_BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/')

    echo "${CURRENT_BRANCH}"
}