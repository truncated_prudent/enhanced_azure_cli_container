FROM ubuntu:20.04

LABEL maintainer='balint.csonka@dmgmori-heitec.hu'

ENV DEBIAN_FRONTEND="noninteractive" 
# ENV TZ="Europe/Budapest"

# Install apk packages
RUN apt-get update
RUN apt-get install -y \
    nano \
    vim \
    maven \
    openjdk-11-jdk-headless \
    tzdata \
    curl \
    git \
    apt-transport-https \
    ca-certificates \
    python3-pip \
    software-properties-common

###
# Terraform
###

RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install terraform

#####
# PRIVATE SSH GITLAB KEY TO IMAGE .SSH
#####

# Create .ssh directory
RUN mkdir /root/.ssh
RUN chmod 700 /root/.ssh
COPY key/id_git_rsa /root/.ssh/id_git_rsa
RUN chmod 600 /root/.ssh/id_git_rsa

COPY config /root/.ssh/config
RUN chmod 600 /root/.ssh/config

# WQ - change pip chardet version to the required version by wq
RUN pip3 install 'chardet<4,>=3.0.2' --force-reinstall
# Add git.werkbliq.de to known_hosts
RUN ssh-keyscan -H git.werkbliq.de > /root/.ssh/known_hosts
# Install wq with pip3
RUN pip3 install git+ssh://git@git.werkbliq.de/backend/wq.git


RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update
RUN apt-get install -y kubectl

# Install Budapest timezone
RUN cp /usr/share/zoneinfo/Europe/Budapest /etc/localtime
RUN echo "Europe/Budapest" >  /etc/timezone

# Cleanup other timezones installed
RUN apt-get -y --purge remove tzdata

# Install kubectl & kubelogin
# RUN az aks install-cli
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# Install helm 3.x
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh

# "Manually" install kubectx and kubens
RUN git clone https://github.com/ahmetb/kubectx.git /tmp/kubectx
RUN cp /tmp/kubectx/kubectx /usr/local/bin
RUN cp /tmp/kubectx/kubens /usr/local/bin
RUN chmod +x /usr/local/bin/kubectx
RUN chmod +x /usr/local/bin/kubens

# Cleanup install files
RUN rm -rf /tmp/kubectx

### Python scripts
RUN mkdir /oper
# RUN git clone....
COPY oper/imagetags.py /oper/imagetags.py

# Create workdir
RUN mkdir /root/workspace



# Copy the script that extends the prompt with the current context and namespace
COPY .kube-prompt.sh /root/.kube-prompt.sh
COPY .git-prompt.sh /root/.git-prompt.sh

# Copy .gitconfig to root's home directory
COPY .gitconfig /root/.gitconfig

# Copy aliases
COPY .aliases /root/.aliases

# .bashrc changes for the beautiful prompt for kubernetes cluster and git
RUN echo '# CUSTOM SHELL\n\
NORMAL="\[\033[00m\]"\nBLUE="\[\033[01;34m\]"\nYELLOW="\[\e[1;33m\]"\nGREEN="\[\e[1;32m\]"\nRED="\[\e[1;31m\]"\n\
source ~/.kube-prompt.sh\n\
source ~/.git-prompt.sh\n\
export PS1="\[\e[33m\][\[\e[m\]\[\e[33m\]\A\[\e[m\]\[\e[33m\]]\[\e[m\] \u@\h:\[\e[36m\]\w\[\e[m\] ${YELLOW}\$(__kube_ps1)${NORMAL} ${RED}\$(__parse_git_branch)${NORMAL} \$ "' >> ~/.bashrc

RUN cat ~/.aliases >> ~/.bashrc

RUN unset DEBIAN_FRONTEND 

WORKDIR /root/workspace

CMD /bin/bash
